const axios = require('axios');
const api = require('lambda-api')();
const mysql = require('mysql')
let AWS = require('aws-sdk')

let PROJECT_ID_CREATE = process.env.PROJECT_ID_CREATE

let PRIVATE_TOKEN = process.env.PRIVATE_TOKEN
let pipelineId = ''
let jobId = ''
let requestId
let ec2InstanceLink
let config

let secretId = "/v1/test/neovendo-database";

let mySQLHost;
let mySQLUser;
let mySQLPassword;
let mySQLDatabase;
let pipelineAndJobIds
let connectionPool;


let passIds = function (req, res, next) {
    req.ec2InstanceLink = ec2InstanceLink
    next()
};


api.use(passIds)
// Create a Secrets Manager client

// In this sample we only handle the specific exceptions for the 'GetSecretValue' API.
// See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
// We rethrow the exception by default.
const client = new AWS.SecretsManager({
    region: "eu-west-1"
});

const getSecrets = async (SecretId) => {
    return await new Promise((resolve, reject) => {
            client.getSecretValue({SecretId}, (err, result) => {
                    if (err) reject(err)
                    else {
                        resolve(JSON.parse(result.SecretString))
                    }
                }
            );
        }
    )
}

async function provideConnectionInfo() {
    const secrets = await getSecrets(secretId);
    if (secrets != null) {
        mySQLHost = secrets.host
        mySQLUser = secrets.username
        mySQLPassword = secrets.password
        mySQLDatabase = secrets.database
    }
}


async function mySQLConnectionPool() {
    await provideConnectionInfo()
    let pool = mysql.createPool({
            connectionLimit: 20,
            host: mySQLHost,
            user: mySQLUser,
            password: mySQLPassword,
            database: mySQLDatabase
        }
    )
    return pool;
}

async function getPipelineIdsAndJobIds() {
    const res = await new Promise(
        function (resolve, reject) {
            connectionPool.getConnection(function (err, connection) {
                    let query = "SELECT DISTINCT pipeline_id,job_id from ec2_request"
                    connection.query(query, function (err, result) {
                            if (err) {
                                console.log(err)
                                throw err
                            } else {
                                pipelineAndJobIds = result
                                connection.release()
                                resolve(pipelineAndJobIds)
                            }
                        }
                    )
                }
            )
        }
    )
    return res
}

async function getRequests(requestId) {
    const res = await new Promise(
        async function (resolve, reject) {
            connectionPool.getConnection(function (err, connection) {
                    let query = "SELECT DISTINCT request_status, instance_link from ec2_request WHERE request_id=?"
                    connection.query(query, [requestId], function (err, result) {
                            if (err) {
                                throw err
                            } else {
                                connection.release()
                                resolve(result)
                            }
                        }
                    )
                }
            )
        }
    )
    return res;
}

api.get('/database-manager/request-status', async (lambdaRequest, lambdaResponse, next) => {
        if (connectionPool == null) {
            connectionPool = await mySQLConnectionPool();
        }
        requestId = lambdaRequest.headers["request-id"]
        const res = await new Promise(
            async function (resolve, reject) {
                let foundRequest = await getRequests(requestId)
                let response
                if (foundRequest != null) {
                    if (foundRequest[0].request_status === "Finished") {
                        response = {
                            "statusCode": 200,
                            "headers": {},
                            "isBase64Encoded": false,
                            "body": foundRequest[0].instance_link
                        }
                    } else {
                        response = {
                            "statusCode": 200,
                            "headers": {},
                            "isBase64Encoded": false,
                            "body": foundRequest[0].request_status
                        }
                    }
                } else {
                    response = {
                        "statusCode": 400,
                        "headers": {},
                        "isBase64Encoded": false,
                        "body": "Request-id doesn't exist"
                    }

                }
                resolve(response)
            }
        )
        return res;
    }
)

api.post('/database-manager/create-request', async (lambdaRequest, lambdaResponse, next) => {
    requestId = lambdaRequest.headers["request-id"]
    pipelineId = lambdaRequest.headers["pipeline-id"]
    jobId = lambdaRequest.headers["job-id"]
    connectionPool = await mySQLConnectionPool();
    const res = await new Promise(
        function (resolve, reject) {
            connectionPool.getConnection(function (err, connection) {
                    let query = "INSERT INTO ec2_request(request_id, pipeline_id,job_id, request_status) VALUES (?,?,?,'running')"
                    connection.query(query, [requestId, pipelineId, jobId], function (err, result) {
                            if (err) {
                                connection.release()
                                console.log("insertion did not work : " + err)
                                let response = {
                                    "statusCode": 404,
                                    "headers": {},
                                    "isBase64Encoded": false,
                                    "body": JSON.stringify({message: "Record not inserted"})
                                }
                                reject(response)

                            } else {
                                connection.release()
                                console.log("1 record inserted")
                                let response = {
                                    "statusCode": 200,
                                    "headers": {},
                                    "isBase64Encoded": false,
                                    "body": JSON.stringify({message: "Record inserted"})
                                }
                                resolve(response)
                            }
                        }
                    )
                }
            )
        }
    )
    return res

})

api.post('/database-manager/gitlab-webhook', async (gitlabRequest, gitlabResponse) => {
    if (connectionPool == null) {
        connectionPool = await mySQLConnectionPool();
    }

    console.log("receiving messages from gitlab webhook")
    await getPipelineIdsAndJobIds();
    for (let index in pipelineAndJobIds) {
        pipelineId = pipelineAndJobIds[index].pipeline_id;
        jobId = pipelineAndJobIds[index].job_id
        if (gitlabRequest.body["object_attributes"].id === pipelineId && gitlabRequest.body["object_attributes"].status === 'success') {
            let localPipelineId = gitlabRequest.body["object_attributes"].id
            config = {
                method: 'GET',
                url: `https://gitlab.com/api/v4/projects/${PROJECT_ID_CREATE}/jobs/${jobId}/trace`,
                headers: {
                    "PRIVATE-TOKEN": PRIVATE_TOKEN
                }
            }
            //Get Job Logs using Job Id
            await axios(config)
                .then(
                    (getJobLogsRequest) => {
                        let jobLog = JSON.stringify(getJobLogsRequest.data)
                        const regex = new RegExp('.*instance-link = (.*.com:[0-9]{4}).*');
                        ec2InstanceLink = jobLog.match(regex)[1]
                    }
                )
            console.log(`localPipelineId : ${localPipelineId} and pipelineId : ${pipelineId}`)
            console.log(`ec2InstanceLink : ${ec2InstanceLink}`)
            connectionPool.getConnection(function (err, connection) {
                    let query = "UPDATE ec2_request SET ?, request_status = 'Finished'  WHERE pipeline_id = ?"
                    connection.query(query, [{instance_link: ec2InstanceLink}, localPipelineId], function (err, result) {
                            if (err) {
                                throw err
                                connection.release();
                            } else {
                                connection.release();
                                console.log("EC2 link provided")
                            }
                        }
                    )
                }
            )
        }
    }
})


exports.handler = async (event, context) => {
    return await api.run(event, context)
}